const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

const userRoutes = require("./routes/user")
const courseRoutes = require("./routes/course")

const port = process.env.PORT || 4000;

const app = express();

app.use(express.json());
app.use(express.urlencoded({extended : true}));
app.use(cors())

mongoose.connect('mongodb+srv://admin:admin123@zuittbootcamp.3f2rk.mongodb.net/course-booking?retryWrites=true&w=majority', {
	useNewUrlParser: true,
	useUnifiedTopology: true
})

let db = mongoose.connection;

	db.on('error', () => console.error.bind(console, "Connection Error"))
	db.once('open', () => console.log('Connected to the cloud database.'))

app.use("/users", userRoutes);
app.use("/courses", courseRoutes);

app.listen(port, () => console.log (`Server is running at port number: ${port}.`))
