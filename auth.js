const jwt = require("jsonwebtoken");
const secret = "CourseBookingAPI";

module.exports.createAccessToken = (user) => {
	// The data will be received from the registration form
	// When the user logs in, a token will be created with user's information.
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin,
	}

	// Generate a JSON web token using the jwt's sign method.
	// Generates the token using the form data, and the secret code with no additional options provided.
	return jwt.sign(data, secret, {})
}

module.exports.verify = (req, res, next) => {

	// The token is retrieved from the request header
	let token = req.headers.authorization;

	// Token received and not undefined.
	if (typeof token !== "undefined") {
		console.log(token)

		// The token sent is a type of "bearer" token which received contains the "bearer" as a prefix to the string
			// Syntax: string.slice(start, end)
		token = token.slice(7,token.length)

		return jwt.verify(token, secret, (err, data) => {
			if (err) {
				return res.send({auth: "failed1"})
			} else {
				next()
			}
		})
	} else {
		return res.send({auth: "failed2"})
	}
}

module.exports.decode = (token) => {
	if(typeof token !== "undefined"){
		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (err,data) => {
			if (err) {
				return null
			} else {
				return jwt.decode(token, {complete:true}).payload
			}
		})
	} else {
		return null
	}
}

module.exports.decodeAdmin = (token) => {
	if(typeof token !== "undefined"){
		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (err,data) => {
			if (err) {
				return null
			} else if (jwt.decode(token, {complete:true}).payload.isAdmin) {
				return true
			} else {
				return false
			}
		})
	} else {
		return null
	}
}
