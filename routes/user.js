const express = require("express");
const router = express.Router();
const userController = require("../controllers/user")
const auth = require("../auth")

router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send (resultFromController))
})

router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send (resultFromController))
})

router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send (resultFromController))
})

// Session 33 Mini-Activity

/*router.post("/details", (req, res) => {
	userController.getProfile(req.body).then(resultFromController => res.send (resultFromController))
})*/

module.exports = router

// Session 34
router.get("/details", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)

	userController.getProfile(req.body).then(resultFromController => res.send (resultFromController))
})
