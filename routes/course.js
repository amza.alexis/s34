const express = require("express");
const router = express.Router();
const courseController = require("../controllers/course")
const auth = require("../auth")

router.post("/", (req,res) => {
	if (auth.decodeAdmin(req.headers.authorization)) {
	courseController.addCourse(req.body);
	res.send(`User is admin, course successfully created!`)
	}
	
	else {res.send(`User is not admin, cannot create course.`)}
})

module.exports = router;
